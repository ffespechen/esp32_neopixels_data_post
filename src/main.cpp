#include <Arduino.h>

#include <Adafruit_NeoPixel.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

// Parámetros de conexión
const char *ssid = "xxxxxxxxxxxxx";
const char *psswrd = "xxxxxxxxxxxxx";
const char *server = "http://ip:5000";

const int PIN = 19; // Pin de conexión de datos al Neopixel
const int LEDS = 8; // Cantidad de LEDS del Neopixel

// Pines de COLORES
const int PIN_R = 25;
const int PIN_G = 26;
const int PIN_B = 27;

const int PIN_W = 32; // Pin para ENCENDIDO TOTAL
const int PIN_O = 33; // Pin para APAGADO TOTAL
const int PIN_L = 13; // Cantidad de LEDs

// Variables para el rgb
int r = 50;
int g = 50;
int b = 50;
// Variable para cantidad de LEDs encendidos
int leds_encendidos = 0;

// Parameter 1 = number of pixels in tiraled
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)
Adafruit_NeoPixel tiraled = Adafruit_NeoPixel(LEDS, PIN, NEO_GRB + NEO_KHZ800);

LiquidCrystal_I2C lcd(0x27, 16, 2);

void interruptCallback()
{

  if (digitalRead(PIN_R) == HIGH)
  {
    r = r + 5;
  }

  if (digitalRead(PIN_G) == HIGH)
  {
    g = g + 5;
  }

  if (digitalRead(PIN_B) == HIGH)
  {
    b = b + 5;
  }

  if (r > 255)
  {
    r = 255;
  }

  if (g > 255)
  {
    g = 255;
  }

  if (b > 255)
  {
    b = 255;
  }

  if (digitalRead(PIN_W) == HIGH)
  {
    r = 255;
    g = 255;
    b = 255;
  }

  if (digitalRead(PIN_O) == HIGH)
  {
    r = 0;
    g = 0;
    b = 0;
  }

  if (digitalRead(PIN_L) == HIGH)
  {
    if (leds_encendidos < LEDS)
    {
      leds_encendidos++;
    }
  }

  Serial.print("R: ");
  Serial.print(r);
  Serial.print(" G: ");
  Serial.print(g);
  Serial.print(" B: ");
  Serial.print(b);
  Serial.println();
}

void setup()
{
  Serial.begin(115200);

  // Inicializando la tira de Neopixel
  tiraled.begin();
  tiraled.setBrightness(50);
  tiraled.show(); // Initialize all pixels to 'off'

  pinMode(PIN_R, INPUT_PULLDOWN);
  pinMode(PIN_G, INPUT_PULLDOWN);
  pinMode(PIN_B, INPUT_PULLDOWN);
  pinMode(PIN_W, INPUT_PULLDOWN);
  pinMode(PIN_O, INPUT_PULLDOWN);
  pinMode(PIN_L, INPUT_PULLDOWN);

  attachInterrupt(digitalPinToInterrupt(PIN_R), interruptCallback, RISING);
  attachInterrupt(digitalPinToInterrupt(PIN_G), interruptCallback, RISING);
  attachInterrupt(digitalPinToInterrupt(PIN_B), interruptCallback, RISING);
  attachInterrupt(digitalPinToInterrupt(PIN_W), interruptCallback, RISING);
  attachInterrupt(digitalPinToInterrupt(PIN_O), interruptCallback, RISING);
  attachInterrupt(digitalPinToInterrupt(PIN_L), interruptCallback, RISING);

  // Inicializo el LCD
  lcd.init();
  lcd.backlight();
  lcd.clear();

  // Inicializo la conexión al WiFi
  WiFi.begin(ssid, psswrd);
  lcd.print("Conectando");

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    lcd.print(".");
  }

  // Si se conecta, lo muestro en el LCD
  lcd.clear();
  lcd.setCursor(1, 0);
  lcd.print("Conectado al WiFi");
  lcd.setCursor(1, 1);
  lcd.print(WiFi.localIP());
  delay(2000);
  lcd.clear();
}

void loop()
{
  for (int i = 0; i < leds_encendidos; i++)
  {
    tiraled.setPixelColor(i, tiraled.Color(r, g, b));
  }

  tiraled.show();

  // LCD para mostrar la conexión
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(r);
  lcd.setCursor(5, 0);
  lcd.print(g);
  lcd.setCursor(10, 0);
  lcd.print(b);

  // Si está conectada, envío al servidor
  if (WiFi.status() == WL_CONNECTED)
  {

    HTTPClient http;
    http.begin(server);

    http.addHeader("Content-Type", "application/json");

    String httpRequestData = "{ \"R\": " + (String)r + ", \"G\": " + (String)g + ", \"B\": " + (String)b + ", \"ID\": \"" + WiFi.macAddress() + "\"}";

    Serial.println(httpRequestData);

    int httpResponseCode = http.POST(httpRequestData);

    lcd.setCursor(0, 1);
    lcd.print("Response: " + (String)httpResponseCode);

    http.end();
  }
  else
  {
    lcd.setCursor(0, 1);
    lcd.print("Desconectado del WiFi");
  }

  delay(2000);
}
